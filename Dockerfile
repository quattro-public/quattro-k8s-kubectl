FROM centos:7
MAINTAINER "The Quattro Project Team"

ENV K8S_VERSION v1.11.7

RUN curl -L https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl
