# Quattro Platform Kubernetes-kubectl

## Steps to build locally:

1. Build docker image:
  ```
  docker build -t="registry.gitlab.com/quattro/quattro-k8s-kubectl" .
  ```
## Usage:
You can use this container in your gitlab-ci pipeline for communicating with a kubernetes custer. In your .gitlab-ci.yml add the following example for connecting with k8s and deploying an application:

```
k8s-deploy:
  stage: deploy
  image: registry.gitlab.com/quattro/quattro-k8s-kubectl
  script:
    - kubectl --server="$K8S_URL" --token="$K8S_TOKEN" --insecure-skip-tls-verify create -f kube-deployment.yml
```
The $variables are defined in the gitlab-project. Add these under "variables" in project-settings.

Below the content of kube-deployment.yml

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: quattro-canary
  namespace: default
spec:
  revisionHistoryLimit: 3
  minReadySeconds: 10
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1
      maxSurge: 3
  replicas: 2
  template:
    metadata:
      labels:
        app: quattro-canary
    spec:
      containers:
      - name: quattro-canary
        image: registry.gitlab.com/quattro/quattro-canary:latest
        ports:
        - containerPort: 8080
```
